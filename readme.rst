
********************
2D Region Extraction
********************

This library takes lines and extracts non-overlapping regions.

The intent is to support tessellation of overlapping regions,
without degenerate shapes.


Algorithm
=========

Logic for extraction is as follows:

- Calculate all intersections
  *(this module is not concerned with exactly how).*
- Collect unique intersections (in a hash-map for eg).
- Order all junctions with 3 or more edges.
  (note, this could be done at any time).
- Extract 'starting point' candidates

  Typically this is the left most points on any of their connecting lines::

     |   +
     |  /
     | + <- this
     |  \
     |   +

  Or, for the edge cases where lines align, are the vertex with the lower y value::

     | +
     | |
     | +-+
     | ^ this

  Note: need to define how degenerate cases behave
  *(lines that double back on themselves).*

- Iterate over candidates the lowest (x, y), first.
  (sorted or using a min-heap).

  - For each starting point - walk over the region,
    turning left at each junction until the loop is closed.
    *(or an end-point is found, if the input allows this case).*

    - Tag vertices not to walk over them again (or use them as starting points).

    - Once complete, go back to looking for the left-most candidates.


TODO
====

Currently this works as intended, some TODO's remain.

- Co-linear edge support, where edges exactly overlap we likely need to use special merging
  also need tests that do this.
- Optional quantizing of input to avoid float precision error.
