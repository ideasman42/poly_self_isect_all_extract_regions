
'''
See readme for exact logic.
'''

__all__ = (
    'polys_extract_regions_from_edges',
    'polys_extract_regions_from_segments_no_isect',
    'polys_extract_regions_from_segments_isect',
    'polys_extract_regions_from_polygons',
)

def line_point_side_of(l1, l2, pt):
    return (((l1[0] - pt[0]) * (l2[1] - pt[1])) -
            ((l2[0] - pt[0]) * (l1[1] - pt[1])))


def dot(a, b):
    return a[0] * b[0] + a[1] * b[1];


def sub(a, b):
    return (
        a[0] - b[0],
        a[1] - b[1],
    )


def angle_signed(v1, v2):
    from math import atan2
    perp_dot = (v1[1] * v2[0]) - (v1[0] * v2[1])
    return atan2(perp_dot, dot(v1, v2))

# -----------------------------------------------------------------------------
# Local utils

# Trim dangling lines before extracting regions
# Needed unless there are no dangling lines

def write_obj(filepath, verts, edges):
    with open(filepath, 'w') as fh:
        fw = fh.write
        for v in verts:
            fw('v %f %f 0.0\n' % v)
        for e in edges:
            fw('l %d %d\n' % (e[0] + 1, e[1] + 1))


def polys_to_verts(polygons):
    return [
        p
        for poly in polygons
        for p in poly
    ]


def polys_to_edges(polygons):
    edges = []
    offset = 0
    for poly in polygons:
        i_prev = offset + (len(poly) - 1)
        for i in range(offset, offset + len(poly)):
            edges.append((i_prev, i))
            i_prev = i
        offset += len(poly)
    return edges


def segments_to_verts_and_edges(segments):
    verts_unique = {}
    for s in segments:
        for p in s:
            if p not in verts_unique:
                verts_unique[p] = len(verts_unique)
    verts = [None] * len(verts_unique)
    for k, v in verts_unique.items():
        verts[v] = k

    edges = [(verts_unique[s[0]], verts_unique[s[1]]) for s in segments]
    return verts, edges


def edges_subdivide_by_isect(verts, edges, isect):
    '''
    Currently modifies verts/edges in place.
    better practice to copy.
    '''
    if not isect:
        return verts, edges

    # edge_index: [isect, ...]
    isect_edges = {}

    # (x, y): vert_index
    isect_verts = {}
    offset = len(verts)
    # Needed because some intersections can be at 'T' junctions,
    # where we only want to consider one of the edges in the intersection.
    isect_verts_new = 0

    def find_vertex(ix, e_pair):
        # Most likely this returns None.
        # Needed because a point may intersect an edge ('T' junction mentioned above).
        for e_index in e_pair:
            for v_index in edges[e_index]:
                if verts[v_index] == ix:
                    return v_index, e_index
        return None, None

    for ix, e_pair in isect:
        v_index, e_index_for_vert = find_vertex(ix, e_pair)
        if v_index is None:
            v_index = offset + isect_verts_new
            isect_verts_new += 1
            e_side = -1
        ix_edges = isect_verts.setdefault(ix, (v_index, []))
        # add four vertices for the two edges.
        # TODO: degenerate edges that double back could add the same vert twice.
        ix_edges[1].extend([i for e_index in e_pair for i in edges[e_index]])

        for e_index in e_pair:
            assert(e_index < len(edges))
            if e_index != e_index_for_vert:
                isect_edges.setdefault(e_index, []).append(ix_edges)

    # Extend verts
    verts.extend([None] * isect_verts_new)
    for ix, (index, _) in isect_verts.items():
        verts[index] = ix

    # Subdivide edges
    for e_index, ix_edges_list in isect_edges.items():
        # sort the intersections
        assert(e_index < len(edges))
        i0, i1 = edges[e_index]
        if len(ix_edges_list) > 1:
            xd = verts[i1][0] - verts[i0][0]
            yd = verts[i1][1] - verts[i0][1]
            if abs(xd) >= abs(yd):
                ix_edges_list.sort(key=lambda ix: verts[ix[0]][0], reverse=(xd < 0.0))
            else:
                ix_edges_list.sort(key=lambda ix: verts[ix[0]][1], reverse=(yd < 0.0))
            del xd, yd
        next_index = 1
        i_prev = i0
        for i_isect, other_verts in ix_edges_list:
            if next_index != 1:
                other_verts[other_verts.index(i0)] = i_prev
            if next_index != len(ix_edges_list):
                other_verts[other_verts.index(i1)] = ix_edges_list[next_index]
            i_prev = i_isect
            next_index += 1

        # modify edges in-place, we could/should modify a copy, but in this case it seems OK not to.
        offset = len(edges)
        edges.extend([None] * len(ix_edges_list))
        i_prev = i0
        for i_isect, other_verts in ix_edges_list:
            edges[offset] = i_prev, i_isect
            offset += 1
            i_prev = i_isect
        # swap the middle in-place
        edges[e_index] = i_prev, i1
    # OBJ
    if False:
        for v in verts:
            print('v', v[0], v[1], '0.0')
        for e in edges:
            print('l', e[0] + 1, e[1] + 1)

    return verts, edges


def polys_extract_regions_from_edges(verts, edges, use_trim=True):
    '''
    This could be merged into 'edges_subdivide_by_isect'
    and optimized to avoid re-creating junctions, for now use as-is.
    '''

    # Slow, only for ensuring input is valid.
    assert(len(set(tuple(sorted(e)) for e in edges)) == len(edges))

    other = [[] for i in range(len(verts))]
    for i0, i1 in edges:
        if (i0 != i1):
            other[i0].append(i1)
            other[i1].append(i0)

    # Can't use from now on!
    del edges

    if use_trim:
        for i, o in enumerate(other):
            while len(o) == 1:
                j = o[0]
                o.clear()
                o = other[j]
                o.remove(i)
                i = j
        for i, o in enumerate(other):
            assert(len(o) != 1)
        # We may have cleared all links,
        # continue as normal - to ensure empty case is also working.

    # Write out OBJ for edges we're using
    if False:
        edges_final = []
        edge_set = set()
        edges_final = []
        for i, o in enumerate(other):
            for j in o:
                l = len(edge_set)
                edge_set.add((i, j) if i < j else (j, i))
                if l != len(edge_set):
                    edges_final.append(ekey)

        write_obj('/tmp/out.obj', verts, edges_final)
        del edges_final, edge_set


    def is_start_candidate(i_curr):
        n = 0
        for i in other[i_curr]:
            if verts[i_curr] < verts[i]:
                if n == 1:
                    return True
                n += 1
        return False

    verts_flag = [0] * len(verts)
    # This could be optimized easily, for now leave as-is.
    maybe_start_here = []
    for i in range(len(verts)):
        if is_start_candidate(i):
            maybe_start_here.append(i)

    # sort by xy
    maybe_start_here.sort(key=lambda i: verts[i])

    regions = []


    def side_of(l0, l1, p):
        return line_point_side_of(verts[l0], verts[l1], verts[p])


    # walk region!
    def pick_best_index(i_curr):
        i_best = -1
        for i in other[i_curr]:
            if verts[i_curr] > verts[i]:
                pass
            elif (i_best == -1) or side_of(i_curr, i_best, i) > 0.0:
                i_best = i
        return i_best

    def pick_best_index_next(i_prev, i_curr):
        i_best = -1
        for i in other[i_curr]:
            if i != i_prev:
                if i_best == -1:
                    i_best = i
                else:
                    '''
                    if side_of(i_prev, i_curr, i_best) > side_of(i_prev, i_curr, i):
                        i_best = i
                    '''

                    angle_best = angle_signed(
                        sub(verts[i_prev], verts[i_curr]),
                        sub(verts[i_curr], verts[i_best]))
                    angle_test = angle_signed(
                        sub(verts[i_prev], verts[i_curr]),
                        sub(verts[i_curr], verts[i]))

                    if angle_best < angle_test:
                        i_best = i

        # assert(i_best != -1)
        return i_best

    # for i_test in maybe_start_here:
    #     print(verts[i_test])

    for i_test in maybe_start_here:

        if is_start_candidate(i_test):
            # print(verts[i_test])
            i_curr = pick_best_index(i_test)
            if i_curr != -1:
                i_prev = i_test
                assert(i_curr != -1)
                r = [i_prev]
                while True:
                    # print(" ", verts[i_curr])
                    assert(i_curr != -1)
                    try:
                        other[i_prev].remove(i_curr)
                    except ValueError:
                        # should never happen
                        assert(0)
                        r = None
                        break
                    # # Allow walking in the opposite direction
                    # # not so common, but two shapes may share an edge with opposite winding.
                    try:
                        other[i_curr].remove(i_prev)
                    except ValueError:
                        # should never happen
                        assert(0)
                        r = None
                        break
                    if i_curr == i_test:
                        # print(" ", "# OK")
                        break
                    r.append(i_curr)
                    i_next = pick_best_index_next(i_prev, i_curr)
                    i_prev = i_curr
                    i_curr = i_next

                    # Check for degenerate
                    # depends on input
                    if i_curr == -1:
                        # print(" ", "# NOT OK", len(other[i_prev]))
                        r = None
                        break

                if r is not None:
                    regions.append(r)
    return regions


def polys_extract_regions_from_segments_no_isect(segments):
    '''
    Wrapper for 'polys_extract_regions_from_edges'.
    '''

    verts, edges = segments_to_verts_and_edges(segments)
    regions = polys_extract_regions_from_edges(verts, edges)
    return verts, regions


def polys_extract_regions_from_segments_isect(segments):
    '''
    Take a list of closed (x, y)
    '''
    import poly_point_isect

    verts, edges = segments_to_verts_and_edges(segments)

    # intersections can be edge pairs
    isect = poly_point_isect.isect_segments(verts, edges)
    # print(isect)
    # print(len(isect))

    verts, edges = edges_subdivide_by_isect(verts, edges, isect)

    regions = polys_extract_regions_from_edges(verts, edges)
    return verts, regions


def polys_extract_regions_from_polygons(polygons):
    '''
    Take a list of closed (x, y)
    '''
    import poly_point_isect
    verts = polys_to_verts(polygons)
    edges = polys_to_edges(polygons)

    # intersections can be edge pairs
    isect = poly_point_isect.isect_segments(verts, edges)

    verts, edges = edges_subdivide_by_isect(verts, edges, isect)

    polygons = polys_extract_regions_from_edges(verts, edges)
    return verts, regions
