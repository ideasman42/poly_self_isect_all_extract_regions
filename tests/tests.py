
'''
Example of watching a single test:
  watch -n2 'USE_SVG=1 nice -n 20 pypy3 -m unittest tests.IsectTest.test_suzzane'
'''

import sys
import os
import unittest

# Export results to SVG?
# USE_SVG = os.environ.get('USE_SVG')
USE_SVG = True

'''
# Slow (so disable by default)
USE_ROTATION = os.environ.get('USE_ROTATION')
# number of rotations to check
ROTATION_DIV = 20
'''

POLY_ISECT_MODULE_PATH = os.path.join(os.path.dirname(__file__), '..')
TEST_DATA_PATH = os.path.join(os.path.dirname(__file__), 'data')

sys.path.append(POLY_ISECT_MODULE_PATH)
sys.path.append(TEST_DATA_PATH)

import poly_point_isect
import poly_extract_regions

def export_svg(name, s, verts, regions):
    # write svg's to tests dir for now
    dirname = os.path.join(TEST_DATA_PATH, '..', 'data_svg')
    os.makedirs(dirname, exist_ok=True)
    fp = os.path.join(dirname, name + '.svg')
    scale = 512.0
    margin = 1.1
    stroke_width = 0.5
    with open(fp, 'w', encoding='utf-8') as f:
        fw = f.write
        fw('<?xml version="1.0" encoding="UTF-8"?>\n')
        fw('<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1 Tiny//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11-tiny.dtd">\n')
        fw('<svg version="1.1" '
           'width="%d" height="%d" '
           'viewBox="%d %d %d %d" '
           'xmlns="http://www.w3.org/2000/svg">\n' %
           (
            # width, height
            int(margin * scale * 2), int(margin * scale * 2),
            # viewBox
            -int(margin * scale), -int(margin * scale), int(margin * scale * 2), int(margin * scale * 2),
            ))

        fw('<rect x="%d" y="%d" width="%d" height="%d" fill="black"/>\n' %
            (-int(margin * scale), -int(margin * scale), int(margin * scale * 2), int(margin * scale * 2),
            ))

        if s:
            fw('<g stroke="white" stroke-opacity="0.25" stroke-width="%f">\n' %
               (stroke_width,)
            )
            for v0, v1 in s:
                fw('<line x1="%.4f" y1="%.4f" x2="%.4f" y2="%.4f" />\n' %
                (v0[0] * scale, -v0[1] * scale, v1[0] * scale, -v1[1] * scale))
            fw('</g>\n')
        if regions:
            # regions = [regions[3]]
            fw('<g fill="yellow" fill-opacity="0.25" stroke="white" stroke-opacity="0.5" stroke-width="%f">\n' %
               (stroke_width,)
            )
            for poly in regions:
                fw('<polygon points="')
                for i in poly:
                    fw(
                        '%.4f,%.4f ' %
                       (verts[i][0] * scale, -verts[i][1] * scale)
                    )
                fw('"/>\n')
            fw('</g>\n')

        fw('</svg>')


def test_data_load(name):
    return __import__(name).data


def segments_rotate(s, angle):
    from math import sin, cos
    si = sin(angle)
    co = cos(angle)

    def point_rotate(x, y):
        return (x * co - y * si,
                x * si + y * co)

    s = [(point_rotate(*p0), point_rotate(*p1)) for (p0, p1) in s]

    return s


def extract_regions_no_isect(segments):
    verts, regions = poly_extract_regions.polys_extract_regions_from_segments_no_isect(segments)
    return verts, regions


def extract_regions_isect(segments):
    verts, regions = poly_extract_regions.polys_extract_regions_from_segments_isect(segments)
    return verts, regions


def internal_extract_regions(mode, name, result=None, rotate=0.0):
    s = test_data_load(name)
    if rotate != 0.0:
        from math import radians
        s = segments_rotate(s, radians(rotate))
        del radians

    if mode == 'NO_ISECT':
        verts, regions = extract_regions_no_isect(s)
    elif mode == 'ISECT':
        verts, regions = extract_regions_isect(s)
    else:
        raise Exception(f'Mode {mode!r} not known')

    if USE_SVG:
        name_svg = name
        if rotate != 0.0:
            name_svg = name_svg + '_r' + str(rotate)
        export_svg(name_svg, s, verts, regions)
        del name_svg
    return verts, regions


class _TestDataFile_ExtractRegionAny_Helper:
    def assertTestData(self, name, result=None, rotate=0.0):
        verts, regions = internal_extract_regions(self._mode, name, result, rotate)
        result_test = tuple(sorted([len(r) for r in regions]))
        result_test = tuple((l, result_test.count(l)) for l in sorted(set(result_test)))
        if result != result_test:
            print(name)
            print(result_test)
        self.assertEqual(result, result_test)


class TestDataFile_ExtractRegionNoIsect_Helper(_TestDataFile_ExtractRegionAny_Helper):
    _mode = 'NO_ISECT'


class TestDataFile_ExtractRegionIsect_Helper(_TestDataFile_ExtractRegionAny_Helper):
    _mode = 'ISECT'


class SimpleNoIsect(unittest.TestCase, TestDataFile_ExtractRegionNoIsect_Helper):
    """
    Tests that don't have any intersections
    (ensure we have no false positives).
    """

    def test_noisect_square(self):
        self.assertTestData('test_noisect_square', result=((4, 1),))

    def test_noisect_square_4(self):
        self.assertTestData('test_noisect_square_4', result=((4, 4),))

    def test_noisect_circle_5(self):
        self.assertTestData('test_noisect_circle_5', result=((32, 5),))

    def test_noisect_circle_5_r45(self):
        self.assertTestData('test_noisect_circle_5', result=((32, 5),), rotate=45.0)

    def test_noisect_complex(self):
        self.assertTestData('test_noisect_complex', result=((4, 2), (6, 2), (8, 1)))

    def test_noisect_complex_r45(self):
        self.assertTestData('test_noisect_complex', result=((4, 2), (6, 2), (8, 1)), rotate=45.0)


class SimpleIsect(unittest.TestCase, TestDataFile_ExtractRegionIsect_Helper):
    """
    Tests that have intersections.
    """

    def test_square(self):
        self.assertTestData('test_square', result=((4, 1),))

    def test_square_4(self):
        self.assertTestData('test_square_4', result=((4, 4),))

    def test_circle_5(self):
        self.assertTestData('test_circle_5', result=((32, 5),))

    def test_complex(self):
        self.assertTestData(
            'test_complex',
            result=((3, 10), (4, 7), (5, 3), (9, 1), (12, 1)),
        )

    def test_complex_2(self):
        self.assertTestData(
            'test_complex_2',
            result=(
                (3, 23), (4, 32), (5, 17), (6, 8), (7, 2), (9, 1), (11, 1),
            )
        )

    def test_complex_3(self):
        self.assertTestData(
            'test_complex_3',
            result=(
                (3, 536), (4, 540), (5, 244), (6, 72), (7, 24), (11, 4), (16, 4)
            ),
        )

    # NOTE: this is quite slow, consider disabling it!
    def test_complex_4(self):
        self.assertTestData(
            'test_complex_4',
            result=(
                (3, 41988), (4, 51012), (5, 24056), (6, 6948), (7, 1444), (8, 316), (9, 16), (11, 4), (12, 8), (15, 4)
            ),
        )

class DegenerateNoIsect(unittest.TestCase, TestDataFile_ExtractRegionIsect_Helper):
    """
    Tests that contain no closed regions and are degenerate (many loose ends).
    """
    pass

    def test_degenerate_line_horizontal(self):
        self.assertTestData('test_degenerate_line_horizontal', result=(),)

    def test_degenerate_line_vertical(self):
        self.assertTestData('test_degenerate_line_vertical', result=(),)

    def test_degenerate_line_diagonal(self):
        self.assertTestData('test_degenerate_line_diagonal', result=(),)

    def test_degenerate_line_multi(self):
        self.assertTestData('test_degenerate_line_multi', result=(),)

    def test_degenerate_line_multi_complex(self):
        self.assertTestData('test_degenerate_line_multi_complex', result=(),)


class DegenerateIsect(unittest.TestCase, TestDataFile_ExtractRegionIsect_Helper):
    '''
    Mix degenerate with regular intersections.
    '''

    def test_degenerate_mix_square_01(self):
        self.assertTestData('test_degenerate_mix_square_01', result=((3, 1), (4, 2)),)

    def test_degenerate_mix_square_02(self):
        self.assertTestData('test_degenerate_mix_square_02', result=((4, 6),),)

    def test_degenerate_mix_square_03(self):
        self.assertTestData('test_degenerate_mix_square_03', result=((4, 6),),)

    def test_degenerate_mix_circle_5(self):
        self.assertTestData('test_degenerate_mix_circle_5', result=((10, 1), (16, 1), (32, 3)),)

    def test_degenerate_mix_complex(self):
        self.assertTestData('test_degenerate_mix_complex', result=((3, 9), (4, 10), (5, 10), (6, 3), (7, 1), (9, 1)),)


if __name__ == '__main__':
    unittest.main(exit=False)

    # little sanity check to see if we miss checking any data
    test_data_real = set([
        f for f in os.listdir(TEST_DATA_PATH)
        if f.endswith('.py')
    ])
    test_data_used = set([
        os.path.basename(m.__file__) for m in sys.modules.values()
        if getattr(m, '__file__', '').startswith(TEST_DATA_PATH)
    ])
    test_data_unused = test_data_real - test_data_used
    if test_data_unused:
        print('Untested modules:')
        for f in sorted(test_data_unused):
            print('   ', f)
