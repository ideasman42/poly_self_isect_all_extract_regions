
'''
blender -b test_data.blend --python test_data_write.py

or

/src/blender/blender.bin -b test_data.blend --python test_data_write.py
'''

import os

def clean_float(text):
    # strip trailing zeros: 0.000 -> 0.0
    index = text.rfind(".")
    if index != -1:
        index += 2
        head, tail = text[:index], text[index:]
        tail = tail.rstrip("0")
        text = head + tail
    return text


import bpy

TEST_DATA_PATH = os.path.join(os.path.dirname(__file__), "data")

import bpy
for ob in bpy.context.scene.objects:
    if ob.type != 'MESH':
        continue
    name = ob.name

    filepath = os.path.join(TEST_DATA_PATH, 'test_' + name + '.py')
    print('writing:', filepath)
    with open(filepath, 'w') as fh:
        fw = fh.write
        fw('data = (\n')
        me = ob.data
        verts = ['(%s, %s)' % tuple(clean_float('%.4f' % a) for a in v.co.xy) for v in me.vertices]
        for e in me.edges:
            v0, v1 = e.vertices
            fw('(%s, %s),\n' % (verts[v0], verts[v1]))
        fw(')\n')
